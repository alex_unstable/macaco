#import "Tweet.h"
#import "User.h"
#import "_TwitterObject.h"

static NSDictionary *_defaultMapping;

@interface Tweet ()

// Private interface goes here.

@end


@implementation Tweet

+(NSDictionary *)defaultMapping
{
    if (!_defaultMapping) {
        _defaultMapping = @{@"created_at" : TweetAttributes.createdAt, @"id" : TweetAttributes.identifier, @"text" : TweetAttributes.text, @"source" : TweetAttributes.source};
    }
    return _defaultMapping;
}

+ (_TwitterObject *)objectWithTwitterData:(NSDictionary *)data andEntityName:(NSString *)entityName inManagedObjectContext:(NSManagedObjectContext *)context
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"%@ Twitter objects must call their designated initialisers", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
    return nil;
}

+ (Tweet *)tweetWithTwitterData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping;
{
    if (!mapping) mapping = [Tweet defaultMapping];
    _TwitterObject *tweet = [_TwitterObject objectWithTwitterData:data andEntityName:@"Tweet" withMapping:mapping inManagedObjectContext:context];
    User *user = [User extractUserFromTwitData:data inManagedObjectContext:context withMapping:nil];
    if (!user){
        DLog(@"Couldn't retrieve user from tweet. Malformed data?");
        return nil;
    }
    if ([tweet isKindOfClass:[Tweet class]]){
            [(Tweet *)tweet setValue:user forKey:TweetRelationships.fromUser];
            return (Tweet *)tweet;
    }
    else {
        DLog(@"The object retrieved differs from expected type. Malformed data?");
        return nil;
    }
}

+ (void)insertTweetWithTwitterData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping;
{
    if (!mapping) mapping = [Tweet defaultMapping];
    _TwitterObject *tweet = [_TwitterObject objectWithTwitterData:data andEntityName:@"Tweet" withMapping:mapping inManagedObjectContext:context];
    User *user = [User extractUserFromTwitData:data inManagedObjectContext:context withMapping:nil];
    if (!user){
        DLog(@"Couldn't retrieve user from tweet. Malformed data?");
    }
    if ([tweet isKindOfClass:[Tweet class]]){
        [(Tweet *)tweet setValue:user forKey:TweetRelationships.fromUser];
    }
    else {
        DLog(@"The object retrieved differs from expected type. Malformed data?");
    }
    
}

+ (NSSortDescriptor *)createStandardSortDescriptor
{
//TODO: Implement
    return nil;
}


@end
