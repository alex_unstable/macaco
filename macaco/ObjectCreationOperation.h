//
//  ObjectCreationOperation.h
//  macaco
//
//  Created by Alex B on 02/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^CompletionBlock)(BOOL success, NSError *error);

@interface ObjectCreationOperation : NSOperation {
    NSArray *_dataDictionaries;
    Class _class;
}

@property (nonatomic, copy) CompletionBlock completionBlock;
@property (nonatomic, weak) NSManagedObjectContext *mainContext;

- (id)initWithDictionaries:(NSArray *)arrayOfDictionaries forClass:(Class)class;

@end

