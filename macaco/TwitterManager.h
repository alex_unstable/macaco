//
//  TwitterManager.h
//  macaco
//
//  Created by Alex B on 02/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^ManagerCompletionBlock)(NSArray *array, NSError *error);


@class TwitterFetcher;
@interface TwitterManager : NSObject

@property (strong, nonatomic) TwitterFetcher *fetcher;

+ (id)sharedInstance;

- (void)getUserTimelineWithCompletionBlock:(ManagerCompletionBlock)completion;
- (void)getUserMessagesWithCompletionBlock:(ManagerCompletionBlock)completion;
- (void)getUserInfoForUserIdentifier:(NSNumber *)userIdentifier andCompletionBlock:(ManagerCompletionBlock)completion;

- (void)sendNewTweetWithText:(NSString *)text andCompletionBlock:(ManagerCompletionBlock)completion;

@end
