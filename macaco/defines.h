//
//  defines.h
//  macaco
//
//  Created by Alex B on 01/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#ifndef macaco_defines_h
#define macaco_defines_h
#import <Foundation/Foundation.h>

//Tweet
#define FromUserSearchKeysDataMapping @{@"from_user" : @"screen_name" ,@"from_user_id" : @"id",@"from_user_name" : @"name"};
#define ToUserSearchKeysDataMapping @{@"to_user" : @"screen_name" ,@"to_user_id" : @"id",@"to_user_name" : @"name"};

//Message
#define SenderUserKeysDataMapping @{@"sender_screen_name" : @"screen_name" ,@"to_user_id" : @"id",@"to_user_name" : @"name"};
#define ToUserKeysDataMapping @{@"to_user" : @"screen_name" ,@"to_user_id" : @"id",@"to_user_name" : @"name"};


#endif
