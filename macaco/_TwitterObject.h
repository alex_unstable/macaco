//
//  _TwitterObject.h
//  macaco
//
//  Created by Alex Benito on 30/11/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import <CoreData/CoreData.h>


@interface _TwitterObject : NSManagedObject

@property (strong, nonatomic) NSDictionary *tweetData;
@property (strong, nonatomic) NSDictionary *mapping;

+ (_TwitterObject *)objectWithTwitterData:(NSDictionary *)data andEntityName:(NSString *)entityName  withMapping:(NSDictionary *)mapping inManagedObjectContext:(NSManagedObjectContext *)context;
+ (NSSortDescriptor *)createStandardSortDescriptor;
+(NSDictionary *)defaultMapping;

- (void)mapValuesWithData:(NSDictionary *)data;



@end
