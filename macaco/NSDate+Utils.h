//
//  NSDate+Utils.h
//  macaco
//
//  Created by Alex B on 03/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utils)
- (NSString *)getFormattedDate;
@end
