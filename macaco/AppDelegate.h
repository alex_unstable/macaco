//
//  AppDelegate.h
//  macaco
//
//  Created by Alex B on 29/11/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

- (void)saveContext:(BOOL)wait;

@end
