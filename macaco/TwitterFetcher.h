//
//  TwitterFetcher.h
//  macaco
//
//  Created by Alex B on 02/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//


#import <Foundation/Foundation.h>
//parameters are output only:
typedef void(^handleResultsBlock)(id resultsJSON,NSError *error);

@interface TwitterFetcher : NSObject
- (id)init;
- (void)getUserTimelineWithResults:(handleResultsBlock)handleBlock;
- (void)getUserMessagesWithCompletionBlock:(handleResultsBlock)handleBlock;
- (void)getInfoForUserWithIdentifier:(NSNumber *)userIdentifier andCompletionBlock:(handleResultsBlock)handleBlock;
- (void)sendTweetWithText:(NSString *)text andCompletionBlock:(handleResultsBlock)handleBlock;
@end
