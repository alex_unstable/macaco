//
//  errors.h
//  macaco
//
//  Created by Alex B on 02/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#ifndef macaco_errors_h
#define macaco_errors_h

//Error domains
#define MacacoFailedAuthenticateLocalUserError  @"Failed to authenticate local user"
#define MacacoFailedToFetchData                 @"Failed to fetch data: Malformed"
//Error codes
#define MacacoFailedAuthenticationErrorCode     2000
#define MacacoFailedToFetchDataErrorCode        2001
#endif
