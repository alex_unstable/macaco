#import "Message.h"
#import "User.h"

static NSDictionary *_defaultMapping;


@interface Message ()

// Private interface goes here.

@end


@implementation Message

+(NSDictionary *)defaultMapping
{
    if (!_defaultMapping) {
        _defaultMapping = @{@"created_at" : MessageAttributes.createdAt, @"id" : MessageAttributes.identifier, @"text" : MessageAttributes.text};
    }
    return _defaultMapping;
}

// Custom logic goes here.
+ (Message *)messageWithTwitterData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping
{
    if (!mapping) mapping = [Message defaultMapping];
    _TwitterObject *message = [_TwitterObject objectWithTwitterData:data andEntityName:@"Message" withMapping:mapping inManagedObjectContext:context];
    NSArray *users = [User extractUsersFromMessageData:data inManagedObjectContext:context withMapping:mapping];
    if (!users || [users count] < 2) {
        DLog(@"Couldn't retrieve users from message. Malformed data?");
        return nil;
    }
    if ([message isKindOfClass:[Message class]]){
            [(Message *)message setValue:[users objectAtIndex:0] forKey:MessageRelationships.sender];
            [(Message *)message setValue:[users objectAtIndex:1] forKey:MessageRelationships.recipient];
        return (Message *)message;
    }
    else {
        DLog(@"The object retrieved differs from expected type. Malformed data?");
        return nil;
    }
}

+ (void)insertMessageWithTwitterData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping
{
    if (!mapping) mapping = [Message defaultMapping];
    _TwitterObject *message = [_TwitterObject objectWithTwitterData:data andEntityName:@"Message" withMapping:mapping inManagedObjectContext:context];
    NSArray *users = [User extractUsersFromMessageData:data inManagedObjectContext:context withMapping:mapping];
    if (!users || [users count] < 2) {
        DLog(@"Couldn't retrieve users from message. Malformed data?");
        return;
    }
    if ([message isKindOfClass:[Message class]]){
        [(Message *)message setValue:[users objectAtIndex:0] forKey:MessageRelationships.sender];
        [(Message *)message setValue:[users objectAtIndex:1] forKey:MessageRelationships.recipient];
        return;
    }
    else {
        DLog(@"The object retrieved differs from expected type. Malformed data?");
        return;
    }
}

@end
