#import "_Tweet.h"

@interface Tweet : _Tweet {}


+ (Tweet *)tweetWithTwitterData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping;
+ (void)insertTweetWithTwitterData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping;

@end
