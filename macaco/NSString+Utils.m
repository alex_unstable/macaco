//
//  NSString+Utils.m
//  macaco
//
//  Created by Alex B on 01/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "NSString+Utils.h"

@implementation NSString (Utils)

- (NSDate *)getTwitterDate;
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE MMM dd HH:mm:ss Z yyyy"];
    return [dateFormatter dateFromString:self];
}

@end

