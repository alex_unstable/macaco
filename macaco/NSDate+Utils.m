//
//  NSDate+Utils.m
//  macaco
//
//  Created by Alex B on 03/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "NSDate+Utils.h"

@implementation NSDate (Utils)
- (NSString *)getFormattedDate;{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd HH:mm:ss yy"];
    return [dateFormatter stringFromDate:self];
}
@end
