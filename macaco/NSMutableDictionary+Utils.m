//
//  NSMutableDictionary+Utils.m
//  macaco
//
//  Created by Alex B on 01/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "NSMutableDictionary+Utils.h"

@implementation NSMutableDictionary (Utils)

- (void)addEntriesFromDictionary:(NSDictionary *)dictionary applyingFilterToKeys:(NSString *(^)(NSString *key))filter
{
    if(!filter)
        return [self addEntriesFromDictionary:dictionary];
    
    for(NSString *key in [dictionary allKeys])
        [self setObject:[dictionary objectForKey:key] forKey:filter(key)];
}

- (void)addEntriesFromDictionary:(NSDictionary *)dictionary WithMappingForKeys:(NSDictionary *)keyMapping;
{
    if (!keyMapping) {
        return [self addEntriesFromDictionary:dictionary];
    }
    for (NSString *key in [keyMapping allKeys]) {
        if (![[dictionary allKeys] containsObject:key]) continue;
        [self setObject:[dictionary objectForKey:key] forKey:[keyMapping objectForKey:key]];
    }
}

@end
