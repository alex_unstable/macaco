//
//  NSString+Utils.h
//  macaco
//
//  Created by Alex B on 01/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)
- (NSDate *)getTwitterDate;
@end
