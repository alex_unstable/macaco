//
//  ObjectCreationOperation.m
//  macaco
//
//  Created by Alex B on 02/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "ObjectCreationOperation.h"
#import "User.h"
#import "Message.h"
#import "Tweet.h"
#import <CoreData/CoreData.h>
#import <objc/message.h>

@implementation ObjectCreationOperation

- (id)initWithDictionaries:(NSArray *)arrayOfDictionaries forClass:(Class)class {
    if (self = [super init]) {
        _class = class;
        _dataDictionaries = arrayOfDictionaries;
    }
    return self;
}

#pragma mark - Core Data
- (void)main
{
    NSManagedObjectContext *localMOC = nil;
    localMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSConfinementConcurrencyType];
    //No need to use the same persistentStore 
    localMOC.parentContext = self.mainContext;
    
    NSError *error = nil;
    SEL selector = nil;
    if ([NSStringFromClass(_class) isEqualToString:@"Tweet"]) {
        selector = @selector(insertTweetWithTwitterData:inManagedObjectContext:withMapping:);
    } else if ([NSStringFromClass(_class) isEqualToString:@"User"]) {
        selector = @selector(insertUserWithTwitterData:inManagedObjectContext:withMapping:);
    } else if ([NSStringFromClass(_class) isEqualToString:@"Message"]) {
        selector = @selector(insertMessageWithTwitterData:inManagedObjectContext:withMapping:);
    }
    
    for (NSDictionary *dictionaryJSON in _dataDictionaries) {
        if ([dictionaryJSON isKindOfClass:[NSDictionary class]]) {
            //me loves obj-c runtime
            objc_msgSend(_class, selector,dictionaryJSON,localMOC,nil);
        } else {
            NSLog(@"");
        }
    }
    ZAssert([localMOC save:&error], @"Error saving import context: %@\n%@",
            [error localizedDescription], [error userInfo]);
    
    
}
@end
