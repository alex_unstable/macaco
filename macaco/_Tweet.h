// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Tweet.h instead.

#import <CoreData/CoreData.h>
#import "_TwitterObject.h"

extern const struct TweetAttributes {
	__unsafe_unretained NSString *createdAt;
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *source;
	__unsafe_unretained NSString *text;
} TweetAttributes;

extern const struct TweetRelationships {
	__unsafe_unretained NSString *fromUser;
	__unsafe_unretained NSString *toUser;
} TweetRelationships;

extern const struct TweetFetchedProperties {
} TweetFetchedProperties;

@class User;
@class User;






@interface TweetID : NSManagedObjectID {}
@end

@interface _Tweet : _TwitterObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (TweetID*)objectID;





@property (nonatomic, strong) NSDate* createdAt;



//- (BOOL)validateCreatedAt:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* identifier;



@property int64_t identifierValue;
- (int64_t)identifierValue;
- (void)setIdentifierValue:(int64_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* source;



//- (BOOL)validateSource:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* text;



//- (BOOL)validateText:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) User *fromUser;

//- (BOOL)validateFromUser:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) User *toUser;

//- (BOOL)validateToUser:(id*)value_ error:(NSError**)error_;





@end

@interface _Tweet (CoreDataGeneratedAccessors)

@end

@interface _Tweet (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveCreatedAt;
- (void)setPrimitiveCreatedAt:(NSDate*)value;




- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int64_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int64_t)value_;




- (NSString*)primitiveSource;
- (void)setPrimitiveSource:(NSString*)value;




- (NSString*)primitiveText;
- (void)setPrimitiveText:(NSString*)value;





- (User*)primitiveFromUser;
- (void)setPrimitiveFromUser:(User*)value;



- (User*)primitiveToUser;
- (void)setPrimitiveToUser:(User*)value;


@end
