//
//  NSMutableDictionary+Utils.h
//  macaco
//
//  Created by Alex B on 01/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (Utils)
- (void)addEntriesFromDictionary:(NSDictionary *)dictionary applyingFilterToKeys:(NSString *(^)(NSString *key))filter;
- (void)addEntriesFromDictionary:(NSDictionary *)dictionary WithMappingForKeys:(NSDictionary *)keyMapping;
@end
