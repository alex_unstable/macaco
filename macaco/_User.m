// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to User.m instead.

#import "_User.h"

const struct UserAttributes UserAttributes = {
	.createdAt = @"createdAt",
	.desc = @"desc",
	.followersCount = @"followersCount",
	.friendsCount = @"friendsCount",
	.identifier = @"identifier",
	.location = @"location",
	.name = @"name",
	.partial = @"partial",
	.user = @"user",
	.userURL = @"userURL",
};

const struct UserRelationships UserRelationships = {
	.inbox = @"inbox",
	.mentions = @"mentions",
	.outbox = @"outbox",
	.tweets = @"tweets",
};

const struct UserFetchedProperties UserFetchedProperties = {
};

@implementation UserID
@end

@implementation _User

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"User";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"User" inManagedObjectContext:moc_];
}

- (UserID*)objectID {
	return (UserID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"followersCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"followersCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"friendsCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"friendsCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"partialValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"partial"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic createdAt;






@dynamic desc;






@dynamic followersCount;



- (int16_t)followersCountValue {
	NSNumber *result = [self followersCount];
	return [result shortValue];
}

- (void)setFollowersCountValue:(int16_t)value_ {
	[self setFollowersCount:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveFollowersCountValue {
	NSNumber *result = [self primitiveFollowersCount];
	return [result shortValue];
}

- (void)setPrimitiveFollowersCountValue:(int16_t)value_ {
	[self setPrimitiveFollowersCount:[NSNumber numberWithShort:value_]];
}





@dynamic friendsCount;



- (int16_t)friendsCountValue {
	NSNumber *result = [self friendsCount];
	return [result shortValue];
}

- (void)setFriendsCountValue:(int16_t)value_ {
	[self setFriendsCount:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveFriendsCountValue {
	NSNumber *result = [self primitiveFriendsCount];
	return [result shortValue];
}

- (void)setPrimitiveFriendsCountValue:(int16_t)value_ {
	[self setPrimitiveFriendsCount:[NSNumber numberWithShort:value_]];
}





@dynamic identifier;



- (int64_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result longLongValue];
}

- (void)setIdentifierValue:(int64_t)value_ {
	[self setIdentifier:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result longLongValue];
}

- (void)setPrimitiveIdentifierValue:(int64_t)value_ {
	[self setPrimitiveIdentifier:[NSNumber numberWithLongLong:value_]];
}





@dynamic location;






@dynamic name;






@dynamic partial;



- (BOOL)partialValue {
	NSNumber *result = [self partial];
	return [result boolValue];
}

- (void)setPartialValue:(BOOL)value_ {
	[self setPartial:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitivePartialValue {
	NSNumber *result = [self primitivePartial];
	return [result boolValue];
}

- (void)setPrimitivePartialValue:(BOOL)value_ {
	[self setPrimitivePartial:[NSNumber numberWithBool:value_]];
}





@dynamic user;






@dynamic userURL;






@dynamic inbox;

	
- (NSMutableSet*)inboxSet {
	[self willAccessValueForKey:@"inbox"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"inbox"];
  
	[self didAccessValueForKey:@"inbox"];
	return result;
}
	

@dynamic mentions;

	

@dynamic outbox;

	
- (NSMutableSet*)outboxSet {
	[self willAccessValueForKey:@"outbox"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"outbox"];
  
	[self didAccessValueForKey:@"outbox"];
	return result;
}
	

@dynamic tweets;

	
- (NSMutableSet*)tweetsSet {
	[self willAccessValueForKey:@"tweets"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"tweets"];
  
	[self didAccessValueForKey:@"tweets"];
	return result;
}
	






@end
