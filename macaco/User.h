#import "_User.h"

@interface User : _User {}

+ (NSArray *)extractUsersFromMessageData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping;

+ (User *)userWithTwitterData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping;
- (void)insertUserWithTwitterData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping;

+ (User *)extractUserFromTwitData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping;


@end
