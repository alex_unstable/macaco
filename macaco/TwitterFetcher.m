//
//  TwitterFetcher.m
//  macaco
//
//  Created by Alex B on 02/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

typedef enum TWRequestMethod {
    TWRequestMethodGET,
    TWRequestMethodPOST,
    TWRequestMethodDELETE
}TWRequestMethod;

#define HOME_TIMELINE       @"https://api.twitter.com/1.1/statuses/home_timeline.json"
#define USER_MESSAGES       @"https://api.twitter.com/1.1/direct_messages.json"
#define USER_INFO           @"https://api.twitter.com/1.1/users/show.json"

#define POST_TWEET          @"https://api.twitter.com/1.1/statuses/update.json"


#import "TwitterFetcher.h"
#import "errors.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface TwitterFetcher ()
- (void)sendSignedURLRequestForURL:(NSURL *)url withRequestMetod:(TWRequestMethod)requestMethod parameters:(NSDictionary *)parameters andCompletionBlock:(handleResultsBlock)handleBlock;
@end

@implementation TwitterFetcher {
    NSOperationQueue *_queue;
    BOOL _isAuthenticated;
    NSError *_lastError;
}
- (id)init
{
    if (self = [super init]) {
        _isAuthenticated = NO;

    }
    return self;
}


- (void)getUserTimelineWithResults:(handleResultsBlock)handleBlock
{
    if (!_queue) {
        _queue = [NSOperationQueue new];
        [_queue setMaxConcurrentOperationCount:1];
    }
    [self sendSignedURLRequestForURL:[NSURL URLWithString:HOME_TIMELINE] withRequestMetod:TWRequestMethodGET parameters:@{@"count" : @"100"} andCompletionBlock:handleBlock];
}

- (void)getUserMessagesWithCompletionBlock:(handleResultsBlock)handleBlock
{
    if (!_queue) {
        _queue = [NSOperationQueue new];
        [_queue setMaxConcurrentOperationCount:1];
    }
    [self sendSignedURLRequestForURL:[NSURL URLWithString:USER_MESSAGES] withRequestMetod:TWRequestMethodGET parameters:nil andCompletionBlock:handleBlock];

}
- (void)getInfoForUserWithIdentifier:(NSNumber *)userIdentifier andCompletionBlock:(handleResultsBlock)handleBlock
{
    if (!_queue) {
        _queue = [NSOperationQueue new];
        [_queue setMaxConcurrentOperationCount:1];
    }
    [self sendSignedURLRequestForURL:[NSURL URLWithString:USER_INFO] withRequestMetod:TWRequestMethodGET parameters:@{@"user_id" : userIdentifier.stringValue} andCompletionBlock:handleBlock];
}

- (void)sendTweetWithText:(NSString *)text andCompletionBlock:(handleResultsBlock)handleBlock
{
    if (!_queue) {
        _queue = [NSOperationQueue new];
        [_queue setMaxConcurrentOperationCount:1];
    }
    [self sendSignedURLRequestForURL:[NSURL URLWithString:POST_TWEET] withRequestMetod:TWRequestMethodPOST parameters:@{@"status" : text} andCompletionBlock:handleBlock];

}

#pragma mark - Private

- (void)sendSignedURLRequestForURL:(NSURL *)url withRequestMetod:(TWRequestMethod)requestMethod parameters:(NSDictionary *)parameters andCompletionBlock:(handleResultsBlock)handleBlock;
{
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *twitterType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:twitterType options:nil completion:^(BOOL granted, NSError *error) {
        if (granted) {
            SLRequestMethod method;
            switch (requestMethod) {
                case TWRequestMethodGET:
                    method = SLRequestMethodGET;
                    break;
                case TWRequestMethodPOST:
                    method = SLRequestMethodPOST;
                    break;
                case TWRequestMethodDELETE:
                    method = SLRequestMethodDELETE;
                    break;
                default:
                    break;
            }
            NSArray *accounts = [accountStore accountsWithAccountType:twitterType];
            ACAccount *account = [accounts lastObject];
            
            SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:method URL:url parameters:parameters];
            [request setAccount:account];
            NSURLRequest *signedURLRequest = request.preparedURLRequest;
            
            [NSURLConnection sendAsynchronousRequest:signedURLRequest queue:_queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                if (data && !error) {
                    id jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                    if (!error && jsonData && ([jsonData isKindOfClass:[NSArray class]] || [jsonData isKindOfClass:[NSDictionary class]])) {
                        handleBlock(jsonData,nil);
                    }
                } else {
                    NSDictionary *userInfo = @{NSUnderlyingErrorKey : error};
                    NSError *baseError  = [NSError errorWithDomain:MacacoFailedToFetchData code:MacacoFailedToFetchDataErrorCode userInfo:userInfo];
                    handleBlock(nil,baseError);
                }
            }];
            
        }
        else {
            NSDictionary *userInfo = @{NSUnderlyingErrorKey : error};
            NSError *error  = [NSError errorWithDomain:MacacoFailedAuthenticateLocalUserError code:MacacoFailedAuthenticationErrorCode userInfo:userInfo];
            handleBlock(nil,error);
        }
    }];
}

@end
