    //
//  TimelineViewController.m
//  macaco
//
//  Created by Alex B on 02/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "TimelineViewController.h"
#import "Tweet.h"
#import "User.h"
#import "AppDelegate.h"
#import "TweetCell.h"
#import "NSDate+Utils.h"
#import "TwitterManager.h"

@interface TimelineViewController ()

@end

@implementation TimelineViewController
@synthesize managedObjectContext=_managedObjectContext;

- (void)setupFetchedResultsController
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Tweet"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:NO]];
    //Fetch all the tweets and populate the table
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                        managedObjectContext:self.managedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TweetCell";
    
    TweetCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // ask NSFetchedResultsController for the Tweet
    Tweet *tweet = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.userLabel.text = [NSString stringWithFormat:@"%@ (%@)",[tweet.fromUser user], [tweet.fromUser name]];

    cell.tweetLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.tweetLabel.numberOfLines = 0;
    cell.tweetLabel.text = tweet.text;
    
    cell.dateLabel.text = [NSString stringWithFormat:@"Gibbered at: %@",[tweet.createdAt getFormattedDate]];
    return cell;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSManagedObjectContext *mainContext = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).managedObjectContext;
    [self setManagedObjectContext:mainContext];
    [self setupFetchedResultsController];
}

- (void)viewWillAppear:(BOOL)animated
{
    [[TwitterManager sharedInstance] getUserTimelineWithCompletionBlock:^(NSArray *array, NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"I couldn't fetch new tweets: Check your connection and try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            });
        }
    }];
}
- (IBAction)newTweetButtonTapped:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *composeAlertView = [[UIAlertView alloc] initWithTitle:@"New Tweet" message:@"Compose your tweet below" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Send", nil];
        composeAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        [composeAlertView show];
    });
}
- (IBAction)refreshTimeLineButtonPressed:(id)sender
{
    [[TwitterManager sharedInstance] getUserTimelineWithCompletionBlock:^(NSArray *array, NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"I couldn't fetch new tweets: Check your connection and try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            });
        }
    }];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            break;
        case 1:
            [[TwitterManager sharedInstance] sendNewTweetWithText:[alertView textFieldAtIndex:0].text andCompletionBlock:nil];
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
