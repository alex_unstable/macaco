#import "_Message.h"

@interface Message : _Message {}

+ (Message *)messageWithTwitterData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping;
+ (void)insertMessageWithTwitterData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping;


@end
