// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to User.h instead.

#import <CoreData/CoreData.h>
#import "_TwitterObject.h"

extern const struct UserAttributes {
	__unsafe_unretained NSString *createdAt;
	__unsafe_unretained NSString *desc;
	__unsafe_unretained NSString *followersCount;
	__unsafe_unretained NSString *friendsCount;
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *location;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *partial;
	__unsafe_unretained NSString *user;
	__unsafe_unretained NSString *userURL;
} UserAttributes;

extern const struct UserRelationships {
	__unsafe_unretained NSString *inbox;
	__unsafe_unretained NSString *mentions;
	__unsafe_unretained NSString *outbox;
	__unsafe_unretained NSString *tweets;
} UserRelationships;

extern const struct UserFetchedProperties {
} UserFetchedProperties;

@class Message;
@class Tweet;
@class Message;
@class Tweet;












@interface UserID : NSManagedObjectID {}
@end

@interface _User : _TwitterObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (UserID*)objectID;





@property (nonatomic, strong) NSDate* createdAt;



//- (BOOL)validateCreatedAt:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* desc;



//- (BOOL)validateDesc:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* followersCount;



@property int16_t followersCountValue;
- (int16_t)followersCountValue;
- (void)setFollowersCountValue:(int16_t)value_;

//- (BOOL)validateFollowersCount:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* friendsCount;



@property int16_t friendsCountValue;
- (int16_t)friendsCountValue;
- (void)setFriendsCountValue:(int16_t)value_;

//- (BOOL)validateFriendsCount:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* identifier;



@property int64_t identifierValue;
- (int64_t)identifierValue;
- (void)setIdentifierValue:(int64_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* location;



//- (BOOL)validateLocation:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* partial;



@property BOOL partialValue;
- (BOOL)partialValue;
- (void)setPartialValue:(BOOL)value_;

//- (BOOL)validatePartial:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* user;



//- (BOOL)validateUser:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* userURL;



//- (BOOL)validateUserURL:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *inbox;

- (NSMutableSet*)inboxSet;




@property (nonatomic, strong) Tweet *mentions;

//- (BOOL)validateMentions:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *outbox;

- (NSMutableSet*)outboxSet;




@property (nonatomic, strong) NSSet *tweets;

- (NSMutableSet*)tweetsSet;





@end

@interface _User (CoreDataGeneratedAccessors)

- (void)addInbox:(NSSet*)value_;
- (void)removeInbox:(NSSet*)value_;
- (void)addInboxObject:(Message*)value_;
- (void)removeInboxObject:(Message*)value_;

- (void)addOutbox:(NSSet*)value_;
- (void)removeOutbox:(NSSet*)value_;
- (void)addOutboxObject:(Message*)value_;
- (void)removeOutboxObject:(Message*)value_;

- (void)addTweets:(NSSet*)value_;
- (void)removeTweets:(NSSet*)value_;
- (void)addTweetsObject:(Tweet*)value_;
- (void)removeTweetsObject:(Tweet*)value_;

@end

@interface _User (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveCreatedAt;
- (void)setPrimitiveCreatedAt:(NSDate*)value;




- (NSString*)primitiveDesc;
- (void)setPrimitiveDesc:(NSString*)value;




- (NSNumber*)primitiveFollowersCount;
- (void)setPrimitiveFollowersCount:(NSNumber*)value;

- (int16_t)primitiveFollowersCountValue;
- (void)setPrimitiveFollowersCountValue:(int16_t)value_;




- (NSNumber*)primitiveFriendsCount;
- (void)setPrimitiveFriendsCount:(NSNumber*)value;

- (int16_t)primitiveFriendsCountValue;
- (void)setPrimitiveFriendsCountValue:(int16_t)value_;




- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int64_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int64_t)value_;




- (NSString*)primitiveLocation;
- (void)setPrimitiveLocation:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSNumber*)primitivePartial;
- (void)setPrimitivePartial:(NSNumber*)value;

- (BOOL)primitivePartialValue;
- (void)setPrimitivePartialValue:(BOOL)value_;




- (NSString*)primitiveUser;
- (void)setPrimitiveUser:(NSString*)value;




- (NSString*)primitiveUserURL;
- (void)setPrimitiveUserURL:(NSString*)value;





- (NSMutableSet*)primitiveInbox;
- (void)setPrimitiveInbox:(NSMutableSet*)value;



- (Tweet*)primitiveMentions;
- (void)setPrimitiveMentions:(Tweet*)value;



- (NSMutableSet*)primitiveOutbox;
- (void)setPrimitiveOutbox:(NSMutableSet*)value;



- (NSMutableSet*)primitiveTweets;
- (void)setPrimitiveTweets:(NSMutableSet*)value;


@end
