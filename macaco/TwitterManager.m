//
//  TwitterManager.m
//  macaco
//
//  Created by Alex B on 02/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "TwitterManager.h"
#import "TwitterFetcher.h"
#import "ObjectCreationOperation.h"
#import "User.h"
#import "Message.h"
#import "Tweet.h"
#import "AppDelegate.h"
#import "errors.h"

@implementation TwitterManager {
    NSOperationQueue *_creationQueue;
}
@synthesize fetcher=_fetcher;

+ (id)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] initWithFetcher:[[TwitterFetcher alloc] init]];
    });
    return _sharedObject;
}

- (id)initWithFetcher:(TwitterFetcher *)fetcher
{
    if (self = [super init]) {
        _fetcher = fetcher;
//        [_fetcher getInfoForUserWithIdentifier:@(14078152) andCompletionBlock:^(id resultsJSON, NSError *error) {
//            NSOperationQueue *aQueue = [NSOperationQueue new];
//            ObjectCreationOperation *creation = [[ObjectCreationOperation alloc] initWithDictionaries:@[resultsJSON] forClass:[User class]];
//            NSManagedObjectContext *mainContext = ((AppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
//            [creation setMainContext:mainContext];
//            [aQueue addOperation:creation];
//        }];
//        [_fetcher getUserMessagesWithCompletionBlock:^(id resultsJSON, NSError *error) {
//            NSOperationQueue *aQueue = [NSOperationQueue new];
//            ObjectCreationOperation *creation = [[ObjectCreationOperation alloc] initWithDictionaries:resultsJSON forClass:[Message class]];
//            NSManagedObjectContext *mainContext = ((AppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
//            [creation setMainContext:mainContext];
//            [aQueue addOperation:creation];
//        }];
        
    }
    
    return self;
}

- (void)getUserTimelineWithCompletionBlock:(ManagerCompletionBlock)completion;
{
    [_fetcher getUserTimelineWithResults:^(NSArray *resultsJSON, NSError *error) {
        if (resultsJSON && !error) {
            if (!_creationQueue) {
                _creationQueue = [NSOperationQueue new];
            }
            ObjectCreationOperation *creation = [[ObjectCreationOperation alloc] initWithDictionaries:resultsJSON forClass:[Tweet class]];
            NSManagedObjectContext *mainContext = ((AppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
            [creation setMainContext:mainContext];
            [_creationQueue addOperation:creation];
            completion(resultsJSON,nil);
        }
        else {
            NSDictionary *userInfo = @{NSUnderlyingErrorKey : error};
            NSError *baseError  = [NSError errorWithDomain:MacacoFailedAuthenticateLocalUserError code:MacacoFailedAuthenticationErrorCode userInfo:userInfo];
            completion(nil,baseError);
        }
    }];
}

- (void)getUserMessagesWithCompletionBlock:(ManagerCompletionBlock)completion
{

}

- (void)getUserInfoForUserIdentifier:(NSNumber *)userIdentifier andCompletionBlock:(ManagerCompletionBlock)completion
{
    
}

- (void)sendNewTweetWithText:(NSString *)text andCompletionBlock:(ManagerCompletionBlock)completion
{
    [_fetcher sendTweetWithText:text andCompletionBlock:^(id resultsJSON, NSError *error) {
        if (!error) {
            DLog(@"Tweet successfully sent");
            if (completion) completion(resultsJSON,nil);
        }
        else {
            NSDictionary *userInfo = @{NSUnderlyingErrorKey : error};
            NSError *baseError  = [NSError errorWithDomain:MacacoFailedAuthenticateLocalUserError code:MacacoFailedAuthenticationErrorCode userInfo:userInfo];
            completion(nil,baseError);
        }
    }];
}

@end
