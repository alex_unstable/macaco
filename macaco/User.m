#import "User.h"
#import "defines.h"
#import "NSMutableDictionary+Utils.h"

@interface User ()

// Private interface goes here.

@end

static NSDictionary *_defaultMapping;

@implementation User

+(NSDictionary *)defaultMapping
{
    if (!_defaultMapping) {
        _defaultMapping = @{@"id" : UserAttributes.identifier, @"name" : UserAttributes.name, @"screen_name" : UserAttributes.user, @"location" : UserAttributes.location, @"description" : UserAttributes.desc, @"url" : UserAttributes.userURL, @"followers_count" : UserAttributes.followersCount, @"friends_count" : UserAttributes.friendsCount};
    }
    return _defaultMapping;
}

+ (_TwitterObject *)objectWithTwitterData:(NSDictionary *)data andEntityName:(NSString *)entityName inManagedObjectContext:(NSManagedObjectContext *)context
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"%@ Twitter objects must call their designated initialisers", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
    return nil;
}

+ (User *)extractUserFromTwitData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping;
{
    return [User userWithTwitterData:[data objectForKey:@"user"] inManagedObjectContext:context withMapping:nil];
}


#pragma mark - Public

+ (NSArray *)extractUsersFromMessageData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping;
{
    //Taking advantage here of the two user objects the friends at Twitter had the consideration to embed in every message
    
    return @[[User userWithTwitterData:[data objectForKey:@"sender"] inManagedObjectContext:context withMapping:nil], [User userWithTwitterData:[data objectForKey:@"recipient"] inManagedObjectContext:context withMapping:nil]];
}


+ (User *)userWithTwitterData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping;
{
    _TwitterObject *user;
    NSDictionary *twitterData = data;
    
    if (!mapping) mapping = [User defaultMapping];
    user = [_TwitterObject objectWithTwitterData:twitterData andEntityName:NSStringFromClass([self class]) withMapping:mapping inManagedObjectContext:context];
    if (user && [user isKindOfClass:[self class]]) {
        ((User *)user).partial = @(NO);
        return (User *)user;
    }
    else if (!user) { DLog(@"The object couldn't be created"); return nil;}
    
    else { DLog(@"The object retrieved differs from expected type. Malformed data?"); return nil;}
}

- (void)insertUserWithTwitterData:(NSDictionary *)data inManagedObjectContext:(NSManagedObjectContext *)context withMapping:(NSDictionary *)mapping
{
    _TwitterObject *user;
    NSDictionary *twitterData = data;
    
    if (!mapping) mapping = [User defaultMapping];
    user = [_TwitterObject objectWithTwitterData:twitterData andEntityName:NSStringFromClass([self class]) withMapping:mapping inManagedObjectContext:context];
    if (user && [user isKindOfClass:[self class]]) {
        ((User *)user).partial = @(NO);
        return;
    }
    else if (!user) { DLog(@"The object couldn't be created"); return;}
    
    else { DLog(@"The object retrieved differs from expected type. Malformed data?"); return;}
    
    
}



@end
