//
//  UsersListViewController.h
//  macaco
//
//  Created by Alex B on 03/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface UsersListViewController : CoreDataTableViewController
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end
