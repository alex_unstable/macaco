//
//  _TwitterObject.m
//  macaco
//
//  Created by Alex Benito on 30/11/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "_TwitterObject.h"
#import "NSMutableDictionary+Utils.h"
#import "NSString+Utils.h"

@implementation _TwitterObject
@synthesize tweetData=_tweetData, mapping=_mapping;



+ (_TwitterObject *)objectWithTwitterData:(NSDictionary *)data andEntityName:(NSString *)entityName  withMapping:(NSDictionary *)mapping inManagedObjectContext:(NSManagedObjectContext *)context;
{
    _TwitterObject *object;
    
    //Sanity check
    if ([[data objectForKey:@"id"] integerValue] == 0)
        return nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entityName];
    request.predicate = [NSPredicate predicateWithFormat:@"identifier = %@", [data objectForKey:@"id"]]; //this is gonna be a key in every single twitter object, by definition, plus it'll give us the chronological order.
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    NSError *error = nil;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
    if (!objects || ([objects count] > 1)) {
        // handle error
    } else if (![objects count]) {
        object = [NSEntityDescription insertNewObjectForEntityForName:entityName
                                             inManagedObjectContext:context];
        [object setMapping:mapping];
        [object mapValuesWithData:data];
    } else {
        object = [objects lastObject];
//TODO: Update object
    }
    
    return object;
}

+ (NSSortDescriptor *)createStandardSortDescriptor
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"%@ can't be called in a root class", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
    return nil;
}

+(NSDictionary *)defaultMapping
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"Root clases don't have a default mapping %@ ", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
    return nil;
    
}

/**
 Map the incoming values into the CD object.
 Save the original fata for later use.
 */
- (void)mapValuesWithData:(NSDictionary *)data;
{
    _tweetData = data;
    NSDictionary *coreDataAttribs = [[self entity] attributesByName];
    NSMutableDictionary *mappedInconmigData = [NSMutableDictionary dictionaryWithCapacity:[data count]];
    [mappedInconmigData addEntriesFromDictionary:data WithMappingForKeys:_mapping];
    for (NSString *attrib in coreDataAttribs) {
        id value = [mappedInconmigData objectForKey:attrib];
        if (!value || [value isKindOfClass:[NSNull class]]) continue;
        //This instronspection definitely could be less hard-coded...
        if ([[[coreDataAttribs objectForKey:attrib] attributeValueClassName] isEqualToString:NSStringFromClass([NSDate class])]) value = [((NSString *)value)getTwitterDate];
        [self setValue:value forKey:attrib];
    }
}


@end
