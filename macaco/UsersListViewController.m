//
//  UsersListViewController.m
//  macaco
//
//  Created by Alex B on 03/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "UsersListViewController.h"
#import "User.h"
#import "AppDelegate.h"

@interface UsersListViewController ()

@end

@implementation UsersListViewController
@synthesize managedObjectContext=_managedObjectContext;


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSManagedObjectContext *mainContext = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).managedObjectContext;
    [self setManagedObjectContext:mainContext];
    [self setupFetchedResultsController];
}

- (void)setupFetchedResultsController
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:NO]];
    //Fetch all the tweets and populate the table
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                        managedObjectContext:self.managedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UserCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    User *user = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ : %@", user.name, user.desc];
    cell.detailTextLabel.text = user.location;
    return cell;
}



@end
