// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Message.h instead.

#import <CoreData/CoreData.h>
#import "_TwitterObject.h"

extern const struct MessageAttributes {
	__unsafe_unretained NSString *createdAt;
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *text;
} MessageAttributes;

extern const struct MessageRelationships {
	__unsafe_unretained NSString *recipient;
	__unsafe_unretained NSString *sender;
} MessageRelationships;

extern const struct MessageFetchedProperties {
} MessageFetchedProperties;

@class User;
@class User;





@interface MessageID : NSManagedObjectID {}
@end

@interface _Message : _TwitterObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MessageID*)objectID;





@property (nonatomic, strong) NSDate* createdAt;



//- (BOOL)validateCreatedAt:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* identifier;



@property int64_t identifierValue;
- (int64_t)identifierValue;
- (void)setIdentifierValue:(int64_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* text;



//- (BOOL)validateText:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) User *recipient;

//- (BOOL)validateRecipient:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) User *sender;

//- (BOOL)validateSender:(id*)value_ error:(NSError**)error_;





@end

@interface _Message (CoreDataGeneratedAccessors)

@end

@interface _Message (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveCreatedAt;
- (void)setPrimitiveCreatedAt:(NSDate*)value;




- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int64_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int64_t)value_;




- (NSString*)primitiveText;
- (void)setPrimitiveText:(NSString*)value;





- (User*)primitiveRecipient;
- (void)setPrimitiveRecipient:(User*)value;



- (User*)primitiveSender;
- (void)setPrimitiveSender:(User*)value;


@end
