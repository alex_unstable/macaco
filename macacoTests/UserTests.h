//
//  UserTests.h
//  macaco
//
//  Created by Alex Benito on 30/11/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "basicCoreDataTest.h"

@class User;
@interface UserTests : basicCoreDataTest
{
    User *_user;
    NSArray *_partialUsers;
}

@end
