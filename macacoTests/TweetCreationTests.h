//
//  TweetCreationTests.h
//  macaco
//
//  Created by Alex B on 02/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "TwitterManager.h"
@class TwitterManager;

@interface TweetCreationTests : SenTestCase {
    TwitterManager *_manager;
    NSConditionLock *_lock;
}

@end
