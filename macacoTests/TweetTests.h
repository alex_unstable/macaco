//
//  TweetTests.h
//  macaco
//
//  Created by Alex Benito on 30/11/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "basicCoreDataTest.h"

@class Tweet;
@interface TweetTests : basicCoreDataTest {
    Tweet *_tweet;
    Tweet *_mention;
}

@end
