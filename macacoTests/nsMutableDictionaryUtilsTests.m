//
//  nsMutableDictionaryUtilsTest.m
//  macaco
//
//  Created by Alex B on 01/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "nsMutableDictionaryUtilsTests.h"
#import "NSMutableDictionary+Utils.h"

@implementation nsMutableDictionaryUtilsTests

- (void)setUp
{
    _dictionary = [NSMutableDictionary dictionary];
}

- (void)testThatKeysAreCorrectlyFiltered
{
    NSDictionary *example = @{@"is_user" : @"Alex", @"is_verified" : @(YES)};
    [_dictionary addEntriesFromDictionary:example applyingFilterToKeys:^NSString *(NSString *key) {
        return [key stringByReplacingOccurrencesOfString:@"is_" withString:@""];
    }];
    STAssertTrue([[_dictionary allKeys] containsObject:@"user"] && [[_dictionary allKeys] containsObject:@"verified"], @"The dictionary keys haven't been properly filtered");
    STAssertEqualObjects([_dictionary objectForKey:@"user"], @"Alex", @"The values for the filtered keys differ from the original ones");
}

- (void)testThatKeysAreCorrectlyMapped
{
    [_dictionary setObject:@"Alex" forKey:@"name"];
    [_dictionary setObject:@"Benito" forKey:@"surname"];
    NSDictionary *mappingExample = @{@"name" : @"real_name", @"surname" : @"real_surname"};
    NSDictionary *example = @{@"name" : @"Alex", @"surname" : @"Benito"};
    [_dictionary addEntriesFromDictionary:example WithMappingForKeys:mappingExample];
    STAssertTrue([[_dictionary allKeys] containsObject:@"real_name"] && [[_dictionary allKeys] containsObject:@"real_surname"], @"The dictionary keys haven't been properly mapped");
    STAssertEqualObjects([_dictionary objectForKey:@"real_name"], @"Alex", @"The values for the mapped keys differ from the original ones");
}

- (void)tearDown
{
    _dictionary = nil;
}

@end
