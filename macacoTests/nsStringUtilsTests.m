//
//  nsStringUtilsTests.m
//  macaco
//
//  Created by Alex B on 01/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "nsStringUtilsTests.h"
#import "NSString+Utils.h"

@implementation nsStringUtilsTests

- (void)setUp
{
    _twitterDateString = @"Tue Mar 04 15:04:03 +0000 2008";
}

- (void)testThatGeneratedDateMatchesExpectedComponents
{
    NSDate *date = [_twitterDateString getTwitterDate];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSTimeZoneCalendarUnit) fromDate:date];
    
    
    STAssertTrue(components.year == 2008,    @"Year differs from the original one");
    STAssertTrue(components.month ==3,       @"Month differs from the original one");
    STAssertTrue(components.day == 4,       @"Day differs from the original one");
    STAssertTrue(components.hour == 15,      @"Hour differs from the original one");
    STAssertTrue(components.minute == 4,       @"Minute differs from the original one");
    STAssertTrue(components.second == 3,       @"Second differs from the original one");

}

- (void)tearDown
{
    _twitterDateString = nil;
}

@end
