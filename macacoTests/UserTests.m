//
//  UserTests.m
//  macaco
//
//  Created by Alex Benito on 30/11/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "UserTests.h"
#import "User.h"
static NSDictionary  * _kInternalTestDictionary;
static NSDictionary  * _kInternalPartialTestDictionary;

@implementation UserTests

- (void)setUp
{
    [super setUp];
    //Code goes here
    _kInternalPartialTestDictionary  = @{@"from_user" : @"ale0xB", @"from_user_id" : @(123456789), @"from_user_name" : @"Alex Benito", @"created_at" : _ktwitterDateString, @"to_user": [NSNull null], @"to_user_id" : @(0), @"to_user_name" : [NSNull null]};
    
    _kInternalTestDictionary = @{@"id" : @(123456789), @"description" : @"A description" ,@"location" : @"The World" , @"screen_name" : @"ale0xB", @"name" : @"Alex Benito", @"created_at" : _ktwitterDateString, @"followers_count" : @(30000), @"friends_count" : @(30000)};

    _partialUsers = [User partialUsersFromTwitData:_kInternalPartialTestDictionary inManagedObjectContext:self.context withMapping:nil];
    
    _user = [User userWithTwitterData:_kInternalTestDictionary inManagedObjectContext:self.context withMapping:nil];
    
    self.twitterObject = _user;
    
    //We need to force saving at this point for this test
    NSError *error = nil;
    NSAssert([self.context save:&error], @"Error saving context: %@\n%@",
             [error localizedDescription], [error userInfo]);
}

//- (void)testThatFullUserUpdatesExistingPartialUser
//{
//    STAssertEqualObjects(_partialUser.friendsCount, _user.friendsCount, @"Partial objects must be updated when full new ones are available");
//}

- (void)testUserHasAnID
{
    STAssertEqualObjects(_user.identifier, @(123456789), @"Users must always have and id");
}

- (void)testThatPartialUserHasAnUserName
{
    STAssertEqualObjects(_user.name, @"Alex Benito", @"Tweets must always have a name");
}

- (void)testThatPartialUserHasANickName
{
    STAssertEqualObjects(_user.user, @"ale0xB", @"Tweets must always have a name");
}

- (void)testThatNullKeysDontGenerateAMentionedUser
{
    STAssertFalse([_partialUsers count] > 1, @"Missing mentioned user should never create a new user");
}

- (void)tearDown
{
    [super tearDown];
    _user =nil;
    _partialUsers = nil;
}

@end
