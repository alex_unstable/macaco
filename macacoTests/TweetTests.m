//
//  TweetTests.m
//  macaco
//
//  Created by Alex Benito on 30/11/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "TweetTests.h"
#import "macacoTests.h"
#import "User.h"
#import "Tweet.h"

#define _ktwitterDateString @"Tue Mar 04 15:04:03 +0000"

static NSDictionary  * _kInternalTestDictionary;
static NSDictionary  * _kInternalTestDictionaryWithTwoUsers;



@implementation TweetTests

- (void)setUp
{
    [super setUp];
    _kInternalTestDictionary = @{@"id" : @(987654321), @"text" : @"Some example test", @"from_user" : @"ale0xB", @"from_user_id" : @(123456789), @"from_user_name" : @"Alex Benito", @"created_at" : _ktwitterDateString};
    
    _kInternalTestDictionaryWithTwoUsers = @{@"id" : @(987654321), @"text" : @"Some example test", @"from_user" : @"ale0xB", @"from_user_id" : @(123456789), @"from_user_name" : @"Alex Benito", @"created_at" : _ktwitterDateString, @"to_user" : @"Somebody", @"to_user_id" : @(31173), @"to_user_name" : @"somebody"};

    
    _tweet = [Tweet tweetWithTwitterData:_kInternalTestDictionary inManagedObjectContext:self.context withMapping:nil];
    self.twitterObject = _tweet;
    
    _mention = [Tweet tweetWithTwitterData:_kInternalTestDictionaryWithTwoUsers inManagedObjectContext:self.context withMapping:nil];
}


//- (void)testThatUserExists
//{
//    STAssertNotNil(_tweet.user, @"Every tweet must have an user associated to it");
//}

- (void)testThatTweetHasAnID
{
    STAssertEqualObjects(_tweet.identifier, @(987654321), @"Tweets must always have and id");
}

- (void)testThatTweetHasText
{
    STAssertEqualObjects(_tweet.text, @"Some example test", @"Tweets must keep the id with which they were initialised");
}

- (void)testThatTweetAlwaysCreatesAUser
{
    STAssertNotNil(_tweet.fromUser, @"Tweets must always create a user linked to them");
}

- (void)testThatExistingTweetIsNotCreatedAgain
{
    Tweet *tweet = [Tweet tweetWithTwitterData:_kInternalTestDictionary inManagedObjectContext:self.context withMapping:nil];
    STAssertTrue([[tweet objectID] isEqual:[_tweet objectID]], @"Tweets created with same data should point to the same concrete object");
}

- (void)testThatTweetCreatesTwoUsersIfNecessary
{
    BOOL exists = _mention.toUser ? YES : NO;
    STAssertTrue(exists, @"Tweets mentioning somebody must create the corresponding user");
}

- (void)tearDown
{
    [super tearDown];
    _tweet = nil;

}

@end
