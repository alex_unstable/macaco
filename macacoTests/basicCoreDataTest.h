//
//  basicCoreDataTest.h
//  macaco
//
//  Created by Alex B on 30/11/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#define _ktwitterDateString @"Tue Mar 04 15:04:03 +0000"

#import <SenTestingKit/SenTestingKit.h>

@class _TwitterObject;
@class NSManagedObjectContext;
@interface basicCoreDataTest : SenTestCase

@property (strong, nonatomic) _TwitterObject *twitterObject;
@property (strong, nonatomic)  NSManagedObjectContext *context;

- (void)setUpCoreDataEnvironmentForTesting;

@end
