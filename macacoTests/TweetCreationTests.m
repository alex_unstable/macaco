//
//  TweetCreationTests.m
//  macaco
//
//  Created by Alex B on 02/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "TweetCreationTests.h"
#import "Tweet.h"

@implementation TweetCreationTests

- (void)setUp
{
    _manager = [TwitterManager sharedInstance];
    _lock = [NSConditionLock new];
}

- (void)testRequestingTimelineCreatesRequest
{
//    [manager getUserTimeline];
//    STAssertTrue([fetcher wasAskedToFetchTimeline], @"The fetcher should have been asked to fetch the user's timeline");
}

- (void)testThatRequestingTimelineInvokesDelegate
{
    [_manager getUserTimelineWithCompletionBlock:^(NSArray *array, NSError *error) {
        [_lock unlockWithCondition:1];
        BOOL arrayOrErrorExist = array || error ? YES : NO;
        STAssertTrue(arrayOrErrorExist, @"A call to the manager should give an output");
    }];
    
    //NSOperations need the runloop to post notifications, this busy-waiting makes it possible.
    BOOL exitRunLoop = NO;
    while (!exitRunLoop)
    {
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
        if ([_lock tryLockWhenCondition:1]) exitRunLoop = YES;
    }
    
  
}

- (void)tearDown
{
    _lock = nil;
    _manager = nil;
}

@end
