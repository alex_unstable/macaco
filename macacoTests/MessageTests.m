//
//  MessageTests.m
//  macaco
//
//  Created by Alex B on 02/12/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "MessageTests.h"
#import "Message.h"
#import "NSString+Utils.h"

static NSDictionary  * _kInternalTestDictionary;

@implementation MessageTests {
}
- (void)setUp
{
    [super setUp];
    _kInternalTestDictionary = @{
    @"sender" : @{@"id" : @(123456789), @"description" : @"A description" ,@"location" : @"The World" , @"screen_name" : @"ale0xB", @"name" : @"Alex Benito", @"created_at" : _ktwitterDateString, @"followers_count" : @(30000), @"friends_count" : @(30000)}
    ,
    @"recipient" : @{@"id" : @(723643472143), @"description" : @"Some guy from Mars" ,@"location" : @"Mars" , @"screen_name" : @"martian", @"name" : @"The Martian", @"created_at" : _ktwitterDateString, @"followers_count" : @(100000000), @"friends_count" : @(1)}
    ,
    @"created_at" : _ktwitterDateString,
    @"id" : @(7272727328746238764),
    @"text" : @"Where are you??? The NASA is looking for you!"
    };
    _message = [Message messageWithTwitterData:_kInternalTestDictionary inManagedObjectContext:self.context withMapping:nil];
    self.twitterObject = _message;
}


- (void)testThatMessageHasASenderAndARecipient
{
    BOOL bothExist = _message.sender && _message.recipient ? YES : NO;
    STAssertTrue(bothExist, @"Every message must have a sender and a recipient");
}

- (void)testThatMessageHasText
{
    BOOL hasText = _message.text ? YES : NO;
    STAssertTrue(hasText, @"All messages must contain some text");
}

- (void)testThatMessageHasCorrectCreatedAtDate
{
    STAssertEqualObjects(_message.createdAt,[_ktwitterDateString getTwitterDate], @"All messages must contain some text");
}

- (void)tearDown
{
    [super tearDown];
    _message =nil;
}

@end
