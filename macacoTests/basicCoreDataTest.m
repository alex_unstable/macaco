//
//  basicCoreDataTest.m
//  macaco
//
//  Created by Alex B on 30/11/2012.
//  Copyright (c) 2012 Alex Benito. All rights reserved.
//

#import "_TwitterObject.h"
#import "basicCoreDataTest.h"
#import <CoreData/CoreData.h>

@implementation basicCoreDataTest
@synthesize context=_context, twitterObject=_twitterObject;

#pragma mark - Setup

-(void)setUp
{
    [self setUpCoreDataEnvironmentForTesting];
}

- (void)setUpCoreDataEnvironmentForTesting
{
    NSArray *bundles = [NSArray arrayWithObject:[NSBundle bundleForClass:[self class]]];
    NSManagedObjectModel *testMom = [NSManagedObjectModel mergedModelFromBundles:bundles];
    STAssertNotNil(testMom, @"ManagedObjectModel is nil");
    NSPersistentStoreCoordinator *testStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:testMom];
    //An in-memory persistent store is always enough for testing (unless you want to test write/read performance, but there are better ways of doing that)
    STAssertTrue([testStoreCoordinator addPersistentStoreWithType:NSInMemoryStoreType configuration:nil URL:nil options:nil error:NULL] ? YES : NO, @"Should be able to add in-memory store");
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] init];
    moc.persistentStoreCoordinator = testStoreCoordinator;
    self.context = moc;
}

#pragma mark - Testing
- (void)testThatTweetHasAnID
{
    BOOL hasAnID = [_twitterObject valueForKey:@"identifier"] != @(0) ? YES : NO;
    STAssertTrue(hasAnID, @"Tweet objects must always have and id");
}

- (void)tearDown
{
    NSError *error = nil;
    NSAssert([self.context save:&error], @"Error saving context: %@\n%@",
             [error localizedDescription], [error userInfo]);
    
    self.context = nil;
}
@end
