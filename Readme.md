
- I've used mogenerator (http://rentzsch.github.com/mogenerator/) to generate the model classes instead of leaving Xcode to do so. I prefer this method to the categories one, as it adds some more helpful stuff, like safe type attributes and so on.

- I've used Social.Framework: Yes, I cheated. But only a little bit. I'm just using it to get the signed NSURLRequest with the OAuth tokens stored in the user's account. No where else.

- I'm using two NSManagedObjectContexts a la "NSManagedDocument", the main one is executed 
on the main thread, its parent on a private queue (so all saves to disk occur in the background).

- Tests aren't implemented in the latest stages! 
And that's how buggy software gets released when there are tight time constraints. Ask Microsoft what happened with Windows Vista! :trollface 

- Why do you do things sometimes in a way and sometimes in another?
To proof I know both ways

- The UI sucks.
True, but that wasn't on the top of the list of things I wanted to show developing this test. 

- Why Macaco? 
Macaco is spanish for macaque: A small type of monkey. I found it quite appropriate for the occasion.
